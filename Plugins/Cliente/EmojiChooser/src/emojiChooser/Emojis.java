/**
 * EmojiChooser
 *
 * @author  Jovani P�rez Dami�n (@BlackBlex)
 * @license General Public License (GPLv3) | http://www.gnu.org/licenses/
 * @package emojiChooser
 *
 * ==============Information==============
 *      Filename: Emojis.java
 * ---------------------------------------
*/

package emojiChooser;

import com.blackblex.libs.application.components.customs.jLabelImage;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.File;
import java.io.FilenameFilter;

public class Emojis extends javax.swing.JDialog {

    private jLabelImage emojiSelected = null;

    public Emojis() {
        initComponents();
        this.setTitle("Seleccione su emoji");
        this.setModal(true);
        jScrollPane1.setViewportView(jPanel1);
        jScrollPane1.getViewport().setView(jPanel1);
        jScrollPane1.getVerticalScrollBar().setUnitIncrement(16);

        File directory = new File(System.getProperty("user.dir") + "/resources/emojis/");

        File []files = directory.listFiles(new FilenameFilter() {

                @Override
                public boolean accept(File dir, String name) {
                    return name.endsWith(".png");
                }
            });

        for( File file : files )
        {
            jLabelImage jimg = new jLabelImage("/resources/emojis/" + file.getName(), new Dimension(50,50));

            jimg.addMouseListener(new MouseListener(){
                @Override
                public void mouseClicked(MouseEvent e) {
                    jLabelImage jimg = (jLabelImage) e.getComponent();
                    jLabelImage img = new jLabelImage(jimg.getPath(), new Dimension(20,20));
                    img.setCursor(Cursor.getDefaultCursor());

                    emojiSelected = img;

                    dispose();
                }

                @Override
                public void mousePressed(MouseEvent e) {
                }

                @Override
                public void mouseReleased(MouseEvent e) {
                }

                @Override
                public void mouseEntered(MouseEvent e) {
                }

                @Override
                public void mouseExited(MouseEvent e) {
                }

            });

            jPanel1.add(jimg);
        }

        double alto = (double)((double)files.length/4);
        int valor1 = (int) Math.ceil(alto) ;
        int valor = valor1 * 57;
        jPanel1.setPreferredSize(new java.awt.Dimension(246, valor));
        jPanel1.repaint();
        jPanel1.revalidate();

    }

    public jLabelImage showDialog()
    {
        this.setVisible(true);
        return this.emojiSelected;
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {

        jScrollPane1 = new javax.swing.JScrollPane();
        jPanel1 = new javax.swing.JPanel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setAlwaysOnTop(true);

        jScrollPane1.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        jScrollPane1.setToolTipText("");

        jPanel1.setMaximumSize(new java.awt.Dimension(246, 110));
        jPanel1.setPreferredSize(new java.awt.Dimension(246, 110));
        jScrollPane1.setViewportView(jPanel1);

        getContentPane().add(jScrollPane1, java.awt.BorderLayout.CENTER);

        setSize(new java.awt.Dimension(272, 211));
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
}

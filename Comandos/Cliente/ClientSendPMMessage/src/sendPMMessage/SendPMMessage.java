/**
 * ClientSendPMMessage
 *
 * @author  Jovani P�rez Dami�n (@BlackBlex)
 * @license General Public License (GPLv3) | http://www.gnu.org/licenses/
 * @package sendPMMessage
 *
 * ==============Information==============
 *      Filename: SendPMMessage.java
 * ---------------------------------------
*/

package sendPMMessage;

import com.blackblex.libs.core.interfaces.custom.CommandInterface;
import com.blackblex.libs.net.objects.SocketMessage;
import com.blackblex.libs.net.objects.SocketUsername;
import com.blackblex.libs.net.objects.jMessage;

public class SendPMMessage implements CommandInterface
{

    @Override
    public String getDescription()
    {
        return "Manda un mensaje a un usuario en especifico";
    }

    @Override
    public boolean canExecute(SocketUsername socketUsername)
    {
        return true;
    }

    @Override
    public void execute(SocketUsername socketUsername, SocketMessage dataInput)
    {
        SocketUsername user = null;

        System.out.println(dataInput.getFrom() + " - " + dataInput.getTo() );

        for ( int i = 0; i < Plugin.jListUserModel.getSize(); i++ )
        {
            user = (SocketUsername) Plugin.jListUserModel.getElementAt(i);
            if ( user.getIdSession() == dataInput.getFrom() )
            {
                break;
            }
        }

        boolean found = false;

        jMessage jmes;
        for ( ChatFramePM chat : Plugin.chatMPS )
        {
            if ( (chat.getUserMP().getUsername() == user.getUsername()) )
            {
                found = true;
                jmes = new jMessage(1);
                jmes.setJMessage(dataInput.getjMessage());
                chat.jMessagesChat.add(jmes);
                chat.setModel();
                break;
            }
        }

        if ( !found )
        {
            ChatFramePM chatMP = new ChatFramePM();
            chatMP.setUserMP(user);
            jmes = new jMessage(2);
            jmes.setMessage("Se ha establecido un chat privado con " + user.getUsername());
            chatMP.jMessagesChat.add(jmes);
            chatMP.setModel();
            chatMP.setVisible(true);
            Plugin.chatMPS.add(chatMP);
        }

    }

}

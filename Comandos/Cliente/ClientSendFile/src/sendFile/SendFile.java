/**
 * ClientSendFile
 *
 * @author  Jovani P�rez Dami�n (@BlackBlex)
 * @license General Public License (GPLv3) | http://www.gnu.org/licenses/
 * @package sendFile
 *
 * ==============Information==============
 *      Filename: SendFile.java
 * ---------------------------------------
*/

package sendFile;

import com.blackblex.libs.core.interfaces.custom.CommandInterface;
import com.blackblex.libs.net.objects.SocketMessage;
import com.blackblex.libs.net.objects.SocketUsername;
import com.blackblex.libs.net.objects.jMessage;
import com.blackblex.plugins.core.Core;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Base64;
import java.util.List;

public class SendFile implements CommandInterface
{

    @Override
    public String getDescription()
    {
        return "Manda un archivo a un usuario";
    }

    @Override
    public boolean canExecute(SocketUsername socketUsername)
    {
        return true;
    }

    @Override
    public void execute(SocketUsername socketUsername, SocketMessage dataInput)
    {
        SocketUsername user = null;

        for ( int i = 0; i < Plugin.jListUserModel.getSize(); i++ )
        {
            user = (SocketUsername) Plugin.jListUserModel.getElementAt(i);
            if ( user.getIdSession() == dataInput.getFrom() )
            {
                break;
            }
        }

        String pathSave = System.getProperty("user.home") + "\\Downloads\\";

        List <jMessage> jMessagesChat = (List <jMessage>) Core.globalService.getObject("JMESSAGESCHAT");
        jMessage jmes = new jMessage(2);
        jmes.setMessage("Recibiste el siguiente archivo: \"" + dataInput.getFileName() + "\" del usuario: " + user.getUsername() + " Se guard� en: " + pathSave + "");
        jMessagesChat.add(jmes);

        try {
            FileOutputStream fileout = new FileOutputStream(pathSave + dataInput.getFileName());
            fileout.write(Base64.getDecoder().decode(dataInput.getFileContent()));
            fileout.close();
        } catch (FileNotFoundException ex) {
        } catch (IOException ex) {
        }
    }

}

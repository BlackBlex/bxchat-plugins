/**
 * FontChooser
 *
 * @author  Jovani P�rez Dami�n (@BlackBlex)
 * @license General Public License (GPLv3) | http://www.gnu.org/licenses/
 * @package fontChooser
 *
 * ==============Information==============
 *      Filename: Plugin.java
 * ---------------------------------------
*/

package fontChooser;

import javax.swing.JButton;
import javax.swing.JTextPane;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;

import com.blackblex.libs.application.components.styles.JButtonStyleFlat;
import com.blackblex.libs.system.utils.Observable;
import com.blackblex.plugins.core.Core;
import com.blackblex.plugins.core.PluginCore;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Plugin extends PluginCore
{

    private String name = "FontChooser",
            author = "BlackBlex",
            description = "Agrega un selector de fuente basico",
            status = "a";

    private int version = 0;

    private double revision = .1;

    private Core.TYPE type = Core.TYPE.COMPLEMENT;
    public JTextPane jMessage;
    public SimpleAttributeSet attrs = new SimpleAttributeSet();

	@Override
	public boolean load()
	{
        Core.Message.printlnStatus(getName().toUpperCase(), getDescription());
        jMessage = (JTextPane) Core.globalService.getObject("JMESSAGE");

        JButton jbuttonFontChooser = new JButton("Fuente");

		JButtonStyleFlat jbuttonFontChooserStyle = new JButtonStyleFlat(jbuttonFontChooser, "#297FB8", 0);
		jbuttonFontChooser.setUI(jbuttonFontChooserStyle);
        jbuttonFontChooser.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0)
			{
				attrs = new SimpleAttributeSet();
		        FontChooser fontc = new FontChooser();
		        String font[] = fontc.showDialog().split("-");
		        if ( !font[0].contains("999") && !font[1].contains("999") )
		        {
		            switch(font[0])
		            {
		                case "1":
		                    StyleConstants.setBold(attrs, true);
		                    break;
		                case "2":
		                    StyleConstants.setItalic(attrs, true);
		                    break;
		                case "3":
		                    StyleConstants.setBold(attrs, true);
		                    StyleConstants.setItalic(attrs, true);
		                    break;
		                case "4":
		                    StyleConstants.setUnderline(attrs, true);
		                    break;
		            }

		            StyleConstants.setFontSize(attrs, Integer.parseInt(font[1]));

		            jMessage.setCharacterAttributes(attrs, true);
		        }

			}

        });

        Core.globalService.addToContainer("jPanelPluginContainer1", jbuttonFontChooser);

		return true;
	}

	@Override
	public boolean start()
	{
		return true;
	}

	@Override
	public boolean end()
	{
		return true;
	}

	@Override
    public String getName()
    {
        return this.name;
    }

    @Override
    public String getAuthor()
    {
        return this.author;
    }

    @Override
    public String getDescription()
    {
        return this.description;
    }

    @Override
    public int getVersion()
    {
        return this.version;
    }

    @Override
    public double getRevision()
    {
        return this.revision;
    }

    @Override
    public String getStatus()
    {
        return this.status;
    }

    @Override
    public Core.TYPE getType()
    {
        return this.type;
    }

    @Override
    public void update(Observable obj)
    {

    }

}

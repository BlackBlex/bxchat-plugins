**BXChat Plugins**
===================


Aquí se alojaran todos los plugins disponibles para [BXChat Client](https://bitbucket.org/BlackBlex/bxchat-client) y [BXChat Server](https://bitbucket.org/BlackBlex/bxchat-server)

--------
**Plugins disponibles:**

 - **SendPMMessage** ~ Implementa un chat privado. [Client/Server]
 - **SendFile** ~ Agrega la posibilidad de mandar archivos a un usuario en especifico. [Client/Server]
 - **EmojiChooser** ~ Agrega un selector de emojis. [Client]
 - **FontChooser** ~ Agrega la posibilidad de cambiar el tamaño, el estilo de la fuente. [Client]

--------

**Nota:** *Se irán añadiendo más plugins conforme se vayan desarrollando*



--------
 BXChat | Chat básico con sistema de plugins

 Plugins escritos en java

 Author: @BlackBlex (BlackBlex)

 License: General Public License (GPLv3) | http://www.gnu.org/licenses/
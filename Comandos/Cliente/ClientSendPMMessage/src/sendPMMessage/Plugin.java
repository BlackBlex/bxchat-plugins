/**
 * ClientSendPMMessage
 *
 * @author  Jovani P�rez Dami�n (@BlackBlex)
 * @license General Public License (GPLv3) | http://www.gnu.org/licenses/
 * @package sendPMMessage
 *
 * ==============Information==============
 *      Filename: Plugin.java
 * ---------------------------------------
*/

package sendPMMessage;

import com.blackblex.libs.core.interfaces.custom.CommandInterface;
import com.blackblex.libs.net.objects.SocketMessage;
import com.blackblex.libs.net.objects.SocketUsername;
import com.blackblex.libs.system.utils.Observable;
import com.blackblex.libs.net.objects.jMessage;
import com.blackblex.plugins.core.Core;
import com.blackblex.plugins.core.PluginCore;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Map;
import javax.swing.DefaultListModel;
import javax.swing.JList;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;

public class Plugin extends PluginCore
{

    private final String name = "SendPMMessage",
            author = "BlackBlex",
            description = "Manda un mensaje a un usuario en especifico",
            status = "a";

    private final int version = 0;

    private final double revision = .1;

    private final Core.TYPE type = Core.TYPE.COMPLEMENT;

    public static SocketUsername socketServer;

    private JPopupMenu jMenuUserList;

    private JList<SocketUsername> jListUsers;

    public static ArrayList<ChatFramePM> chatMPS = new ArrayList<>();
    public static DefaultListModel<SocketUsername> jListUserModel;

    @Override
    public boolean load()
    {
        Core.globalService.addObject("CHATSMP", this.chatMPS);

        socketServer = (SocketUsername) Core.globalService.getObject("SOCKETSERVER");
        this.jListUsers = (JList <SocketUsername>) Core.globalService.getObject("JLISTUSERS");
        this.jMenuUserList = (JPopupMenu) Core.globalService.getObject("JMENUUSERLIST");

        Core.Message.printlnStatus("COMMAND", "New command");
        Core.Message.printlnStatus(getName().toUpperCase(), getDescription());
        Core.Message.printlnStatus("USAGE", "[sendpm] [user] [message]");

        Map<String, CommandInterface> commands = (Map<String, CommandInterface>) Core.globalService.getObject("COMMANDLIST");
        commands.put("sendpm", new SendPMMessage());

        jListUserModel = (DefaultListModel) Core.globalService.getObject("JLISTUSERSMODEL");

        JMenuItem jMenuItem = new JMenuItem();
        jMenuItem.setText("Mandar mensaje privado");
        jMenuItem.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent evt)
            {
                SocketUsername user = jListUsers.getSelectedValue();
                SocketMessage dataOutput = new SocketMessage();
                dataOutput.setAction("sendpm");
                dataOutput.setFrom(socketServer.getIdSession());
                dataOutput.setTo(user.getIdSession());
                socketServer.sendServer(dataOutput);

                ChatFramePM chatMP = new ChatFramePM();
                chatMP.setUserMP(user);
                jMessage jmess = new jMessage(2);
                jmess.setMessage("Se ha establecido un chat privado con " + user.getUsername());
                chatMP.jMessagesChat.add(jmess);
                chatMP.setModel();
                chatMP.setVisible(true);
                chatMPS.add(chatMP);
            }
        });
        this.jMenuUserList.add(jMenuItem);

        return true;
    }

    @Override
    public boolean start()
    {
        return true;
    }

    @Override
    public boolean end()
    {
        return true;
    }

    @Override
    public String getName()
    {
        return this.name;
    }

    @Override
    public String getAuthor()
    {
        return this.author;
    }

    @Override
    public String getDescription()
    {
        return this.description;
    }

    @Override
    public int getVersion()
    {
        return this.version;
    }

    @Override
    public double getRevision()
    {
        return this.revision;
    }

    @Override
    public String getStatus()
    {
        return this.status;
    }

    @Override
    public Core.TYPE getType()
    {
        return this.type;
    }

    @Override
    public void update(Observable obj)
    {
    }
}
